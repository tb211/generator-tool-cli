# @tangbin/generator-tool-cli

## 介绍

非业务代码库脚手架生成器

## Usage

```shell
npx tb-gt-cli --init
```

## Tips

由于 lint-staged 嵌套执行，需要手动修改 `package.json` 中的 `-lint-staged` 为 `lint-staged`
