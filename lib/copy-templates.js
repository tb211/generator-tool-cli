const { copy_folder } = require('./utils');

/**
 * 从template复制文件到指定目录
 * @param {string} config
 */
function copy_templates(config) {
  const source = `${__dirname}/templates`;
  const target = `${process.cwd()}/${config.foldername}`;

  copy_folder(source, target, config);
}

module.exports = copy_templates;
