const fs = require('fs');
const path = require('path');
const ejs = require('ejs');

/**
 * 创建文件夹
 * @param {string} path
 */
function mkdir_sync(path) {
  try {
    fs.mkdirSync(path);
  } catch (error) {
    console.error('创建目录失败', error);
  }
}

/**
 * 复制文件
 * @param {string} src src path
 * @param {string} target target path
 */
function copy_file(src, target) {
  try {
    fs.copyFileSync(src, target);
    // console.log(`文件从 ${src} 到 ${target} 复制成功`);
  } catch (error) {
    console.error(`文件从 ${src} 到 ${target} 复制失败\n`, error);
  }
}

/**
 * 复制文件夹(使用ejs)
 * @param {string} src src path
 * @param {string} target target path
 * @param {Record<string, string>} config
 */
async function copy_folder(src, target, config) {
  try {
    const paths = fs.readdirSync(src);
    if (paths && paths.length > 0) {
      for (let i = 0, len = paths.length; i < len; i++) {
        const _path = paths[i];
        const src_path = path.resolve(src, _path);
        const target_path = path.resolve(target, _path);

        const stat = fs.statSync(src_path);
        if (stat.isDirectory()) {
          if (!fs.existsSync(target_path)) {
            fs.mkdirSync(target_path);
          }
          copy_folder(src_path, target_path);
        } else {
          copy_file(src_path, target_path);
          const str = await ejs.renderFile(target_path, config, {
            async: true,
          });
          fs.writeFileSync(target_path, str);
        }
      }
    }
    // console.log(`文件夹从 ${src} 到 ${target} 复制成功`);
  } catch (error) {
    console.error(`文件夹从 ${src} 到 ${target} 复制失败\n`, error);
  }
}

const utils = {
  copy_folder,
  mkdir_sync,
};

module.exports = utils;
