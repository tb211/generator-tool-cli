const copy_templates = require('./copy-templates');
const create_folder = require('./create_folder');

/**
 * tb-cli: generator a basic cli tool
 * @param {{foldername: string; description: string}} config
 */
function main(config) {
  // 1. create new folder
  create_folder(config.foldername);

  // 2. copy templates
  copy_templates(config);

  // 5. output guide
  console.log(`
Done. Now run:

  cd ${config.foldername}
  npm install
  `);
}

module.exports = main;
