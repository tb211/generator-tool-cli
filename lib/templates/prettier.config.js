const fabric = require('@tangbin/fabric');

/**
 * @type {import('prettier').Options}
 */
const config = {
  ...fabric.prettier,
};

module.exports = config;
