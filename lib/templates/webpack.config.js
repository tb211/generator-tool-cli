const path = require('path');

/**
 * @type {import('webpack').Configuration}
 */
module.exports = {
  target: 'web',
  mode: 'production',
  entry: path.resolve(__dirname, 'src/index.ts'),
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '<%= umd %>.min.js',
    library: {
      name: '<%= name %>',
      type: 'umd',
    },
    clean: true,
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'babel-loader',
        include: [path.resolve(__dirname, 'src')],
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.ts'],
    modules: [path.resolve(__dirname, './'), 'node_modules'],
  },
