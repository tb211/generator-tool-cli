const { mkdir_sync } = require('./utils');

function create_folder(foldername) {
  mkdir_sync(`${process.cwd()}/${foldername}`);
}

module.exports = create_folder;
