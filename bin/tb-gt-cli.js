#!/usr/bin/env node

const { program } = require('commander');
const inquirer = require('inquirer');
const pkg = require('../package.json');
const tb_cli = require('../lib');

program
  .version(pkg.version)
  .name(pkg.name)
  .description(pkg.description)
  .option('--init', 'Run config initialization wizard', false);

program.parse(process.argv);
const options = program.opts();

if (options.init) {
  inquirer
    .prompt([
      {
        type: 'input',
        message: 'Please input foldername',
        name: 'foldername',
        validate: function (input) {
          const done = this.async();
          if (!/^[a-zA-Z].*$/.test(input)) {
            done('foldername must start with letter');
            return;
          }
          done(null, true);
        },
      },
      {
        type: 'input',
        message: 'Please input project description',
        name: 'description',
        default: '',
      },
      {
        type: 'umd js filename',
        message: 'Please input umd js filename',
        name: 'umd',
        default: '',
      },
      {
        type: 'js global name',
        message: 'Please input js global name',
        name: 'name',
        default: '',
      },
    ])
    .then((answers) => {
      tb_cli(answers);
    })
    .catch((error) => {
      console.error('tb-cli execute failed: \n', error);
    });
}
